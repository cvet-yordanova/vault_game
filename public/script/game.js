import { assets, directions } from "./constants.js";
import { getRandomInt, generateZeroOrOne } from "./utils.js";

export default class Game {
    
    constructor(app) {
        this.pixiApp = app;
        this.secretCombination = [];
        this.combinationSteps = [];
    }

    resetCombination () {
        this.secretCombination = [];
        this.combinationSteps = [];
    }

    generateSecretCombination() {
        let rotationPositions = [0, 1, 2, 3, 4, -1];
        let rotationIdx = 0;

        for (let index = 0; index < 3; index++) {
            this.secretCombination[index] = { code: getRandomInt(1, 9), direction: directions[generateZeroOrOne()] };
            for (let p = 0; p < this.secretCombination[index].code; p++) {
                rotationIdx = this.secretCombination[index].direction === 'clockwise' ? rotationIdx+1 : rotationIdx - 1;
                if(rotationIdx < 0) rotationIdx = 5;
                if(rotationIdx > 5) rotationIdx = 0;
                this.combinationSteps.push(rotationPositions[rotationIdx]);
            }
        }
        console.info(this.secretCombination);
    }

    async preload() {
        await PIXI.Assets.load(assets);
    }

    addBackground(app) {
        this.background = PIXI.Sprite.from('background');
        this.background.anchor.set(0.5);
        this.resizeBackground(app);
        app.stage.addChild(this.background);
    }

    resizeBackground(app) {
        let scale = app.screen.width < 800 ? 0.8 : (app.screen.width < 1200) ? 0.9 : 1;
        this.background.height = app.screen.height * scale;
        this.background.scale.x = this.background.scale.y;
      
        this.background.x = app.screen.width / 2;
        this.background.y = app.screen.height / 2;
    }
}