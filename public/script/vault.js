import { Asset } from "./asset.js";

export default class Vault {
    constructor(app) {
        this.vaultContainer = new PIXI.Container();
        this.openVaultContainer = new PIXI.Container();
        this.sparkleBlinksContainer = new PIXI.Container();

        this.addBlinks(app);
        this.addSparkleBlinks(app);
        this.initDoorWithHandle(app);
        this.initOpenDoor(app);
        this.setInteractiveItems();
        app.stage.addChild(this.vaultContainer);
        app.stage.addChild(this.openVaultContainer);

    }

    initOpenDoor(app) {
        this.doorOpen = Asset.create('doorOpen',
            this.calculateOpenDoorPositionX(app),
            app.screen.height / 2,
            0.5, this.calculateOpenDoorWidth(app));

        this.doorOpenShadow = Asset.create('doorOpenShadow',
            this.calculateOpenDoorPositionX(app) + 20,
            app.screen.height / 2 + 20,
            0.5, this.calculateOpenDoorWidth(app) + 20);

        this.openVaultContainer.addChild(this.doorOpenShadow);
        this.openVaultContainer.addChild(this.doorOpen);
        this.openVaultContainer.visible = false;
    }

    closeDoor(timeline) {
        timeline.to(this.openVaultContainer, { pixi: { alpha: 0, visible: false }, duration: 1, ease: "back.inOut" }, 0);
        timeline.to(this.vaultContainer, { pixi: { alpha: 1, visible: true }, duration: 1, ease: "back.inOut" }, 0);
    }

    openDoor(timeline) {
        this.blinks.forEach(blink => { blink.visible = true; blink.alpha = 0 });
        this.sparkleBlinksContainer.visible = true;
        this.openVaultContainer.visible = true;
        this.openVaultContainer.alpha = 0;

        timeline.to(this.openVaultContainer, { pixi: { alpha: 1 }, duration: 3, ease: "back.inOut" }, 1)
            .to(this.sparkleBlinksContainer, { pixi: { alpha: 1 }, duration: 3, ease: "back.inOut" }, 1);
        this.animateSpakleBlink(timeline);
        this.animateSparkles(timeline);
        timeline.to(this.vaultContainer, { pixi: { alpha: 0, visible: false }, duration: 2, ease: "back.inOut" }, 1)
        timeline.to(this.blinks, { pixi: { alpha: 1 }, duration: 3, ease: "back.inOut" }, 2)

    }

    initDoorWithHandle(app) {
        this.door = Asset.create('door',
            app.screen.width / 2 + 20,
            app.screen.height / 2 - 10, 0.5, this.calculateDoorWidth(app));

        this.handleShadow = Asset.create('handleShadow',
            app.screen.width / 2,
            app.screen.height / 2,
            0.5, this.calculateHandleWidth(app));

        this.handle = Asset.create('handle', this.calculateHandlePosition(app).x,
            this.calculateHandlePosition(app).y, 0.5, this.calculateHandleWidth(app)
        );

        this.vaultContainer.addChild(this.door);
        this.vaultContainer.addChild(this.handleShadow);
        this.vaultContainer.addChild(this.handle);
    }

    calculateDoorWidth(app) {
        return app.screen.width < 800 ? 500 : (app.screen.width < 1200) ? 580 : 630;
    }

    calculateOpenDoorWidth(app) {
        return app.screen.width < 800 ? 320 : (app.screen.width < 1200) ? 370 : 400;
    }

    calculateOpenDoorPositionX(app) {
        let position = this.door.x + this.door.width / 2;
        return app.screen.width < 800 ? position + 100 : (app.screen.width < 1200) ? position + 120 : position + 150;
    }


    calculateHandleWidth(app) {
        return app.screen.width < 800 ? 160 : (app.screen.width < 1200) ? 180 : 200;
    }

    calculateHandlePosition(app) {
        return {
            x: app.screen.width < 800 ? app.screen.width / 2 - 5 : app.screen.width / 2 - 10,
            y: app.screen.width < 800 ? app.screen.height / 2 - 10 : app.screen.height / 2 - 10
        }
    }

    getBlinksRelativePositions() {
        return {
            blink1: { sm: { x: -20, y: 0 }, md: { x: -20, y: 0 }, lg: { x: -20, y: 0 } },
            blink2: { sm: { x: -120, y: 2 }, md: { x: -130, y: 2 }, lg: { x: -160, y: 2 } },
            blink3: { sm: { x: 70, y: 100 }, md: { x: 70, y: 100 }, lg: { x: 70, y: 100 } },
        }
    }

    calculateBlinkPositionX(app, blink) {
        let screenWidth = app.screen.width / 2;
        let blinkRelativePositions = this.getBlinksRelativePositions();
        return app.screen.width < 800 ? screenWidth + blinkRelativePositions[blink].sm.x :
            (app.screen.width < 1200) ? screenWidth + blinkRelativePositions[blink].md.x :
                screenWidth + blinkRelativePositions[blink].lg.x
    }

    calculateBlinkPositionY(app, blink) {
        let screenHeight = app.screen.height / 2;
        let blinkRelativePositions = this.getBlinksRelativePositions();
        return app.screen.width < 800 ? screenHeight + blinkRelativePositions[blink].sm.y :
            (app.screen.width < 1200) ? screenHeight + blinkRelativePositions[blink].md.y :
                screenHeight + blinkRelativePositions[blink].lg.y
    }

    resizeVaultDoor(app) {
        this.door.width = this.calculateDoorWidth(app);
        this.door.scale.y = this.door.scale.x;

        this.handle.width = this.calculateHandleWidth(app);
        this.handle.scale.y = this.handle.scale.x;

        this.handleShadow.width = this.calculateHandleWidth(app);
        this.handleShadow.scale.y = this.handleShadow.scale.x;

        this.doorOpen.width = this.calculateOpenDoorWidth(app);
        this.doorOpen.scale.y = this.doorOpen.scale.x;
        this.doorOpenShadow.width = this.calculateOpenDoorWidth(app) + 20;
        this.doorOpenShadow.scale.y = this.doorOpenShadow.scale.x;

        this.blinks[0].x = this.calculateBlinkPositionX(app, 'blink1');
        this.blinks[0].y = this.calculateBlinkPositionY(app, 'blink1');

        this.blinks[1].x = this.calculateBlinkPositionX(app, 'blink2');
        this.blinks[1].y = this.calculateBlinkPositionY(app, 'blink2');

        this.blinks[2].x = this.calculateBlinkPositionX(app, 'blink3');
        this.blinks[2].y = this.calculateBlinkPositionY(app, 'blink3');


        this.door.x = app.screen.width / 2 + 20;
        this.door.y = app.screen.height / 2 - 10;

        this.handleShadow.x = app.screen.width / 2;
        this.handleShadow.y = app.screen.height / 2;

        this.handle.x = this.calculateHandlePosition(app).x;
        this.handle.y = this.calculateHandlePosition(app).y;

        this.doorOpen.x = this.calculateOpenDoorPositionX(app);
        this.doorOpen.y = app.screen.height / 2;

        this.doorOpenShadow.x = this.calculateOpenDoorPositionX(app) + 20;
        this.doorOpenShadow.y = app.screen.height / 2 + 20;
    }

    setInteractiveItems() {
        this.handle.interactive = true;
        this.handleShadow.interactive = true;
        this.door.interactive = true;
        this.doorOpen.interactive = true;
        this.doorOpenShadow.interactive = true;
        this.vaultContainer.interactive = true;
        this.openVaultContainer.interactive = true;
    }

    resetHandleRotation(tl, rotations=1) {
        gsap.fromTo([this.handle, this.handleShadow], { pixi: { rotation: ((rotations + 18) * 60) }, duration: 5, ease: "power2.in" },
       { pixi: { rotation: 0 }, duration: 4, ease: "power2.out" });
  }


    addBlinks(app) {
        let blink1 = Asset.create('blink',
            this.calculateBlinkPositionX(app, 'blink1'),
            this.calculateBlinkPositionY(app, 'blink1'),
            0.5, 150);


        let blink2 = Asset.create('blink',
            this.calculateBlinkPositionX(app, 'blink2'),
            this.calculateBlinkPositionY(app, 'blink2'),
            0.5, 150);

        let blink3 = Asset.create('blink',
            this.calculateBlinkPositionX(app, 'blink3'),
            this.calculateBlinkPositionY(app, 'blink3'),
            0.5, 150);

        blink1.visible = false;
        blink2.visible = false;
        blink3.visible = false;

        this.blinks = [blink1, blink2, blink3];
        app.stage.addChild(blink1);
        app.stage.addChild(blink2);
        app.stage.addChild(blink3);

    }

    addSparkleBlinks(app) {
        this.sparkleBlinks = [];
        for (let i = 0; i < 10; i++) {
            let blink = Asset.create('blink',
                app.screen.width / 2,
                app.screen.height / 2,
                0.5, 200);
            this.sparkleBlinks.push(blink);
            this.sparkleBlinksContainer.addChild(blink);

        }
        this.sparkleBlinksContainer.visible = false;
        app.stage.addChild(this.sparkleBlinksContainer);

    }

    animateSparkles(timeline) {
        timeline.to(this.blinks, {
            pixi: {
                rotation: 360,
                scale: .3
            },
            duration: 4,
            repeat: -1,
            ease: "none",

        }, 0);
    }


    animateSpakleBlink(timeline) {
        timeline.to(this.sparkleBlinks, {
            keyframes: [
                {
                    rotation: 'random(30, -30)',
                    x: 'random([100, 500, 300, 400, 800, 900,1000 ])',
                    y: 'random([100, 500, 300, 400, 800, 900,1000 ])',
                    ease: 'expo.out',
                    duration: 5,
                    stagger: {
                        amount: 0.1,
                    }
                },
                { alpha: 0, delay: -3 },
            ],
        }, 1);
    }


}