export const directions = ["clockwise", "counterclockwise"];
export const assets = [
    { alias: "background", src: "../assets/bg.png" },
    { alias: "door", src: "../assets/door.png" },
    { alias: "doorOpen", src: "../assets/doorOpen.png" },
    { alias: "doorOpenShadow", src: "../assets/doorOpenShadow.png" },
    { alias: "handle", src: "../assets/handle.png" },
    { alias: "handleShadow", src: "../assets/handleShadow.png" },
    { alias: "blink", src: "../assets/blink.png" },
]