/**
 *  min (inclusive) and max (exclusive)
 */
export const getRandomInt = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export const generateZeroOrOne = () => {
    return Math.round(Math.random());
}