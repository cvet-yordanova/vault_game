export class Asset {
    static create(assetName, positionX, positionY, anchor = 0.5, width) {
        let asset = PIXI.Sprite.from(assetName);
        asset.x = positionX;
        asset.y = positionY;
        asset.anchor = anchor;
        asset.width = width;
        asset.scale.y = asset.scale.x;
        return asset;
    }
}