import Game from "./game.js";
import Vault from "./vault.js";

const app = new PIXI.Application();

const game = new Game(app);

let isDragging = false;
let isPointerDown = false;
let currentStep = 0;

let pointerDownY;
let pointerDownX;

let tl = gsap.timeline();
let startAngle;
let angle = 0;
let rot = 0;


(async () => {
    await setup();
    await game.preload();

    game.generateSecretCombination();
    game.addBackground(app);
    game.vault = new Vault(app);


    setOnPointerDownEvent(game.vault.handle);
    setOnPointerMoveEvent(game.vault.handle);
    setOnPointerUpEvent(game.vault.handle);

    window.addEventListener('resize', (e) => {
        game.resizeBackground(app);
        game.vault.resizeVaultDoor(app);
    })
})();


const setOnPointerDownEvent = (handle) => {
    handle.on('pointerdown', function (e) {
        isPointerDown = true;
        pointerDownY = e.client.y;
        pointerDownX = e.client.x;
        startAngle = Math.atan2(e.pageY - (app.screen.height / 2), e.pageX - (app.screen.width / 2));

    });
}


const setOnPointerMoveEvent = (handle) => {
    handle.on('pointermove', function (e) {
        isDragging = isPointerDown;
        if (isPointerDown) {
            let radians = Math.atan2(e.pageY - (app.screen.height / 2), e.pageX - (app.screen.width / 2));
            rot = radians - startAngle;

            game.vault.handle.rotation = angle + rot;
            game.vault.handleShadow.rotation = handle.rotation;
        }

    });
}

const setOnPointerUpEvent = (handle) => {
    handle.on('pointerup', function () {
        isDragging = false;
        isPointerDown = false;
        angle = game.vault.handle.rotation;

        let rotations = Math.round((game.vault.handle.rotation * 180) / Math.PI / 60);

        gsap.to(game.vault.handle, { pixi: { rotation: rotations * 60 }, duration: 1, ease: "back.inOut", onComplete: () => checkCode() });
        gsap.to(game.vault.handleShadow, { pixi: { rotation: rotations * 60 }, duration: 1, ease: "back.inOut" });

        const checkCode = () => {
            if (game.combinationSteps[currentStep] === rotations) {
                console.log('correct');
                if (currentStep + 1 == game.combinationSteps.length) {
                    game.vault.resetHandleRotation(tl, rotations);
                    game.vault.openDoor(tl);
                } else {
                    currentStep++;
                }
            }
            else {
                console.log('incorrect');
                game.vault.resetHandleRotation(tl, rotations);
                game.vault.handle.rotation = 0;
                game.vault.handleShadow.rotation = 0;
                game.resetCombination();
                game.generateSecretCombination();
                angle = 0;
                currentStep = 0;
            }

        }


    });
}

async function setup() {
    gsap.registerPlugin(PixiPlugin);
    PixiPlugin.registerPIXI(PIXI);
    await app.init({ background: "#1099bb", resizeTo: window });
    document.body.appendChild(app.canvas);
}



